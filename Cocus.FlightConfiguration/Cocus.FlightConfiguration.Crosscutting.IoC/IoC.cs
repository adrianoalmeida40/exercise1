﻿namespace Cocus.FlightConfiguration.Crosscutting.IoC
{
    using Cocus.FlightConfiguration.Application.Implementations;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Cocus.FlightConfiguration.Infra.Data.Repositories;
    using Cocus.FlightConfiguration.Infra.Data.Repositories.Implementations;
    using Microsoft.Extensions.DependencyInjection;

    public class IoC
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // Infra - Data
            services.AddScoped<IAircraftRepository, AircraftRepository>();
            services.AddScoped<IAirportRepository, AirportRepository>();
            services.AddScoped<IFlightRepository, FlightRepository>();

            //Service
            services.AddScoped<IAircraftService, AircraftService>();
            services.AddScoped<IAirportService, AirportService>();
            services.AddScoped<IFlightService, FlightService>();
        }
    }
}
