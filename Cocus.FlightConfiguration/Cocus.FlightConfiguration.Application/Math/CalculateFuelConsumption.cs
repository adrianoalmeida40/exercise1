﻿namespace Cocus.FlightConfiguration.Application.Math
{
    public static class CalculateFuelConsumption
    {
        public static double CalculateFuelConsumptionForFlight(double aircraftConsumption, 
                                                               double distanceBetweenAirports,
                                                               double takeoffEffort)
        {
            return (distanceBetweenAirports * aircraftConsumption) + takeoffEffort;
        }
    }
}
