﻿namespace Cocus.FlightConfiguration.Application.Math
{
    using Cocus.FlightConfiguration.Application.DTO;
    using GeoCoordinatePortable;

    public static class CalculateDistances
    {
        public static double CalculateDistanceBetweenAirports(AirportDTO airportDeparture,
                                                               AirportDTO airportArrive)
        {
            var locAirportoDeparture = new GeoCoordinate(airportDeparture.Latitude, airportDeparture.Longitude);
            var locAirportArrive = new GeoCoordinate(airportArrive.Latitude, airportArrive.Longitude);

            double distance = locAirportoDeparture.GetDistanceTo(locAirportArrive); // metres

            return distance/1000;
        }
    }
}
