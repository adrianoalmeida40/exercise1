﻿namespace Cocus.FlightConfiguration.Application.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Application.DTO;

    public interface IAircraftService
    {
        Task<AircraftDTO> GetByIdAsync(Guid id);

        Task<IEnumerable<AircraftDTO>> GetAllAsync();

        Task<AircraftDTO> CreateAsync(AircraftDTO aircraft);

        Task<bool> UpdateAsync(AircraftDTO aircraft);

        Task<bool> DeleteAsync(Guid id);
    }
}
