﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cocus.FlightConfiguration.Application.DTO;

namespace Cocus.FlightConfiguration.Application.Interfaces
{
    public interface IFlightService
    {
        Task<FlightDTO> GetByIdAsync(Guid id);

        Task<Page<FlightDTO>> GetAllAsync(int skip, int take);

        Task<FlightDTO> CreateAsync(FlightCreateDTO flightCreate);

        Task<bool> UpdateAsync(FlightDTO flight);

        Task<bool> DeleteAsync(Guid id);
    }
}
