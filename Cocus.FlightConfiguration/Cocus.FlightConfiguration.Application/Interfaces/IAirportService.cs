﻿namespace Cocus.FlightConfiguration.Application.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Cocus.FlightConfiguration.Application.DTO;
    public interface IAirportService
    {
        Task<AirportDTO> GetByIdAsync(Guid id);

        Task<Page<AirportDTO>> GetAllAsync(int skip, int take);

        IEnumerable<AirportDTO> GetPartial(string partialText);

        Task<AirportDTO> CreateAsync(AirportDTO airport);

        Task<bool> CreateManyAsync(IEnumerable<AirportDTO> airports);

        Task<bool> UpdateAsync(AirportDTO airport);

        Task<bool> DeleteAsync(Guid id);
    }
}
