﻿namespace Cocus.FlightConfiguration.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Domain.Models;

    public static partial class FlightAdapter
    {
        public static Flight ToModel(this FlightDTO flightDTO)
        {
            return new Flight
            {
                Id = flightDTO.Id,
                AircraftId = flightDTO.AircraftId,
                AirportArriveId = flightDTO.AirportArriveId,
                AirportDepartureId = flightDTO.AirportDepartureId,
                DistancieAirports = flightDTO.DistancieAirports,
                FuelConsumption = flightDTO.FuelConsumption,
                NameAircraft = flightDTO.NameAircraft,
                NameAirportArrive = flightDTO.NameAirportArrive,
                NameAirportDeparture = flightDTO.NameAirportDeparture,
                AirportArriveCode = flightDTO.AirportArriveCode,
                AirportDepartureCode = flightDTO.AirportDepartureCode,
                CityArrive = flightDTO.CityArrive,
                CityDeparture = flightDTO.CityDeparture,
                CountryArrive = flightDTO.CountryArrive,
                CountryDeparture = flightDTO.CountryDeparture,
                StateArrive = flightDTO.StateArrive,
                StateDeparture = flightDTO.StateDeparture
            };
        }

        public static IEnumerable<Flight> ToModel(this IEnumerable<FlightDTO> flightDTOs)
        {
            return flightDTOs.Select(flightDTO => flightDTO.ToModel());
        }
    }
}
