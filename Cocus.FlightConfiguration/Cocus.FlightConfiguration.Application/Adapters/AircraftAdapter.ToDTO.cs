﻿namespace Cocus.FlightConfiguration.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Domain.Models;

    public static partial class AircraftAdapter
    {
        public static AircraftDTO ToDTO(this Aircraft aircraft)
        {
            return new AircraftDTO
            {
                Id = aircraft.Id,
                AverageSpeed = aircraft.AverageSpeed,
                ConsumptionPerKm = aircraft.ConsumptionPerKm,
                Name = aircraft.Name,
                TakeoffEffort = aircraft.TakeoffEffort,
                Type = aircraft.Type
            };
        }

        public static IEnumerable<AircraftDTO> ToDTO(this IEnumerable<Aircraft> aircrafts)
        {
            return aircrafts.Select(aircraft => aircraft.ToDTO());
        }
    }
}
