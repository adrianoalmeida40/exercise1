﻿namespace Cocus.FlightConfiguration.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Domain.Models;

    public static partial class AirportAdapter
    {
        public static AirportDTO ToDTO(this Airport airport)
        {
            return new AirportDTO
            {
                Id = airport.Id,                
                City = airport.City,
                Country = airport.Country,   
                ICAO = airport.ICAO,
                Latitude = airport.Latitude,
                Longitude = airport.Longitude,
                Name = airport.Name,              
                Type = airport.Type,
                Carries = airport.Carries,
                Code = airport.Code,
                Direct_Flights = airport.Direct_Flights,
                Elev = airport.Elev,
                Email = airport.Email,
                Phone = airport.Phone,
                Runway_Lenght = airport.Runway_Lenght,
                State = airport.State,
                Tz = airport.Tz,
                Url = airport.Url,
                Woeid = airport.Woeid
            };
        }

        public static IEnumerable<AirportDTO> ToDTO(this IEnumerable<Airport> airports)
        {
            return airports.Select(airport => airport.ToDTO());
        }
    }
}
