﻿namespace Cocus.FlightConfiguration.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Domain.Models;

    public static partial class FlightAdapter
    {
        public static FlightDTO ToDTO(this Flight flight)
        {
            return new FlightDTO
            {
                Id = flight.Id,
                AircraftId = flight.AircraftId,
                AirportArriveId = flight.AirportArriveId,
                AirportDepartureId = flight.AirportDepartureId,
                DistancieAirports = flight.DistancieAirports,
                FuelConsumption = flight.FuelConsumption,
                NameAircraft = flight.NameAircraft,
                NameAirportArrive = flight.NameAirportArrive,
                NameAirportDeparture = flight.NameAirportDeparture,
                AirportArriveCode = flight.AirportArriveCode,
                AirportDepartureCode = flight.AirportDepartureCode,
                CityArrive = flight.CityArrive,
                CityDeparture = flight.CityDeparture,
                CountryArrive = flight.CountryArrive,
                CountryDeparture = flight.CountryDeparture,
                StateArrive = flight.StateArrive,
                StateDeparture = flight.StateDeparture
            };
        }

        public static IEnumerable<FlightDTO> ToDTO(this IEnumerable<Flight> flights)
        {
            return flights.Select(flight => flight.ToDTO());
        }
    }
}
