﻿namespace Cocus.FlightConfiguration.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Domain.Models;    

    public static partial class AircraftAdapter
    {
        public static Aircraft ToModel(this AircraftDTO aircraftDTO)
        {
            return new Aircraft
            {
                Id = aircraftDTO.Id,
                AverageSpeed = aircraftDTO.AverageSpeed,
                ConsumptionPerKm = aircraftDTO.ConsumptionPerKm,
                Name = aircraftDTO.Name,
                TakeoffEffort = aircraftDTO.TakeoffEffort,
                Type = aircraftDTO.Type
            };
        }

        public static IEnumerable<Aircraft> ToModel(this IEnumerable<AircraftDTO> aircraftDTOs)
        {
            return aircraftDTOs.Select(aircraftDTO => aircraftDTO.ToModel());
        }
    }
}
