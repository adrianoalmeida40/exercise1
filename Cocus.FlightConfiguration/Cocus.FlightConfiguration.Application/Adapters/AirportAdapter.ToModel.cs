﻿namespace Cocus.FlightConfiguration.Application.Adapters
{
    using System.Collections.Generic;
    using System.Linq;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Domain.Models;

    public static partial class AirportAdapter
    {
        public static Airport ToModel(this AirportDTO airportDTO)
        {
            return new Airport
            {
                Id = airportDTO.Id,
                City = airportDTO.City,
                Country = airportDTO.Country,
                ICAO = airportDTO.ICAO,
                Latitude = airportDTO.Latitude,
                Longitude = airportDTO.Longitude,
                Name = airportDTO.Name,
                Type = airportDTO.Type,
                Carries = airportDTO.Carries,
                Code = airportDTO.Code,
                Direct_Flights = airportDTO.Direct_Flights,
                Elev = airportDTO.Elev,
                Email = airportDTO.Email,
                Phone = airportDTO.Phone,
                Runway_Lenght = airportDTO.Runway_Lenght,
                State = airportDTO.State,
                Tz = airportDTO.Tz,
                Url = airportDTO.Url,
                Woeid = airportDTO.Woeid
            };
        }

        public static IEnumerable<Airport> ToModel(this IEnumerable<AirportDTO> airportDTOs)
        {
            return airportDTOs.Select(airportDTO => airportDTO.ToModel());
        }
    }
}
