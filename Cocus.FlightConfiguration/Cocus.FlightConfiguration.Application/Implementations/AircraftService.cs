﻿namespace Cocus.FlightConfiguration.Application.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Application.Adapters;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Cocus.FlightConfiguration.Infra.Data.Repositories;

    public class AircraftService : IAircraftService
    {
        private readonly IAircraftRepository aircraftRepository;
        private readonly IFlightRepository flightRepository;

        public AircraftService(IAircraftRepository aircraftRepository,
                               IFlightRepository flightRepository)
        {
            this.aircraftRepository = aircraftRepository;
            this.flightRepository = flightRepository;
        }

        public async Task<AircraftDTO> CreateAsync(AircraftDTO aircraft)
        {
            var createAirCraft = await aircraftRepository.CreateAsync(aircraft.ToModel());

            return createAirCraft.ToDTO();
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            try
            {
                await flightRepository.DeleteForAircraftAsync(id);
            }
            catch(Exception ex)
            {
                throw new Exception("An error occured in delete flights");
            }

            return await aircraftRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<AircraftDTO>> GetAllAsync()
        {
            var aircrafts = await this.aircraftRepository.GetAllAsync();

            return aircrafts?.ToDTO();
        }

        public async Task<AircraftDTO> GetByIdAsync(Guid id)
        {
            var aircraft = await aircraftRepository.GetByIdAsync(id).ConfigureAwait(false);

            return aircraft?.ToDTO();
        }

        public Task<bool> UpdateAsync(AircraftDTO aircraft)
        {
            return aircraftRepository.UpdateAsync(aircraft.ToModel());
        }
    }
}
