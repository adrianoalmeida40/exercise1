﻿namespace Cocus.FlightConfiguration.Application.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Application.Adapters;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Cocus.FlightConfiguration.Infra.Data.Repositories;

    public class AirportService : IAirportService
    {
        private readonly IAirportRepository airportRepository;
        private readonly IFlightRepository flightRepository;

        public AirportService(IAirportRepository airportRepository,
                              IFlightRepository flightRepository)
        {
            this.airportRepository = airportRepository;
            this.flightRepository = flightRepository;
        }

        public async Task<AirportDTO> CreateAsync(AirportDTO airport)
        {
            var createAirport = await airportRepository.CreateAsync(airport.ToModel());

            return createAirport.ToDTO();
        }

        public async Task<bool> CreateManyAsync(IEnumerable<AirportDTO> airports)
        {
            return await airportRepository.CreateManyAsync(airports.ToModel());
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            try
            {
                await flightRepository.DeleteForAirportAsync(id);
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured in delete flights");
            }

            return await airportRepository.DeleteAsync(id);
        }

        public async Task<Page<AirportDTO>> GetAllAsync(int skip, int take)
        {
            var airports = await this.airportRepository.GetAllAsync(skip, take);

            var airportCount = await airportRepository
                .Count()
                .ConfigureAwait(false);

            return new Page<AirportDTO>(
                content: airports.ToDTO(),
                skip: skip,
                take: take,
                total: airportCount);
        }

        public async Task<AirportDTO> GetByIdAsync(Guid id)
        {
            var airport = await airportRepository.GetByIdAsync(id).ConfigureAwait(false);

            return airport?.ToDTO();
        }

        public IEnumerable<AirportDTO> GetPartial(string partialText)
        {
            var airports = airportRepository.GetPartial(partialText);

            return airports.ToDTO();
        }

        public Task<bool> UpdateAsync(AirportDTO airport)
        {
            return airportRepository.UpdateAsync(airport.ToModel());
        }
    }
}
