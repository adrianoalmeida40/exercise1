﻿namespace Cocus.FlightConfiguration.Application.Implementations
{
    using System;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Application.Adapters;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Cocus.FlightConfiguration.Application.Math;
    using Cocus.FlightConfiguration.Infra.Data.Repositories;

    public class FlightService : IFlightService
    {
        private readonly IFlightRepository flightRepository;
        private readonly IAirportRepository airportRepository;
        private readonly IAircraftRepository aircraftRepository;

        public FlightService(IFlightRepository flightRepository,
                             IAirportRepository airportRepository,
                             IAircraftRepository aircraftRepository)
        {
            this.flightRepository = flightRepository;
            this.airportRepository = airportRepository;
            this.aircraftRepository = aircraftRepository;
        }

        public async Task<FlightDTO> CreateAsync(FlightCreateDTO flightCreate)
        {
            var airportArrive = await airportRepository.GetByIdAsync(flightCreate.AirportArriveId).ConfigureAwait(false);
            var airportDeparture = await airportRepository.GetByIdAsync(flightCreate.AirportDepartureId).ConfigureAwait(false);

            var aircraft = await aircraftRepository.GetByIdAsync(flightCreate.AircraftId).ConfigureAwait(false);

            flightCreate.DistancieAirports = CalculateDistances.CalculateDistanceBetweenAirports(airportDeparture.ToDTO(),
                                                                                                 airportArrive.ToDTO());

            flightCreate.FuelConsumption = CalculateFuelConsumption.CalculateFuelConsumptionForFlight(aircraft.ConsumptionPerKm,
                                                                                                      flightCreate.DistancieAirports,
                                                                                                      aircraft.TakeoffEffort);

            var flight = new FlightDTO
            {
                AircraftId = aircraft.Id,
                AirportArriveCode = airportArrive.Code,
                AirportArriveId = airportArrive.Id,
                AirportDepartureCode = airportDeparture.Code,
                AirportDepartureId = airportDeparture.Id,
                CityArrive = airportArrive.City,
                CityDeparture = airportDeparture.City,
                CountryArrive = airportArrive.Country,
                CountryDeparture = airportDeparture.Country,
                DistancieAirports = flightCreate.DistancieAirports,
                FuelConsumption = flightCreate.FuelConsumption,
                NameAircraft = aircraft.Name,
                NameAirportArrive = airportArrive.Name,
                NameAirportDeparture = airportDeparture.Name,
                StateArrive = airportArrive.State,
                StateDeparture = airportDeparture.State
            };

            var createFlight = await flightRepository.CreateAsync(flight.ToModel());

            return createFlight.ToDTO();
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            return flightRepository.DeleteAsync(id);
        }

        public async Task<Page<FlightDTO>> GetAllAsync(int skip, int take)
        {
            var flights = await this.flightRepository.GetAllAsync(skip, take);

            var flightCount = await flightRepository
                .Count()
                .ConfigureAwait(false);

            return new Page<FlightDTO>(
                content: flights.ToDTO(),
                skip: skip,
                take: take,
                total: flightCount);
        }

        public async Task<FlightDTO> GetByIdAsync(Guid id)
        {
            var flight = await flightRepository.GetByIdAsync(id).ConfigureAwait(false);

            return flight?.ToDTO();
        }

        public async Task<bool> UpdateAsync(FlightDTO flight)
        {
            var airportArrive = await airportRepository.GetByIdAsync(flight.AirportArriveId).ConfigureAwait(false);
            var airportDeparture = await airportRepository.GetByIdAsync(flight.AirportDepartureId).ConfigureAwait(false);

            var aircraft = await aircraftRepository.GetByIdAsync(flight.AircraftId).ConfigureAwait(false);

            flight.DistancieAirports = CalculateDistances.CalculateDistanceBetweenAirports(airportDeparture.ToDTO(),
                                                                                           airportArrive.ToDTO());

            flight.FuelConsumption = CalculateFuelConsumption.CalculateFuelConsumptionForFlight(aircraft.ConsumptionPerKm,
                                                                                                flight.DistancieAirports,
                                                                                                aircraft.TakeoffEffort);

            var flightDto = new FlightDTO
            {
                AircraftId = aircraft.Id,
                AirportArriveCode = airportArrive.Code,
                AirportArriveId = airportArrive.Id,
                AirportDepartureCode = airportDeparture.Code,
                AirportDepartureId = airportDeparture.Id,
                CityArrive = airportArrive.City,
                CityDeparture = airportDeparture.City,
                CountryArrive = airportArrive.Country,
                CountryDeparture = airportDeparture.Country,
                DistancieAirports = flight.DistancieAirports,
                FuelConsumption = flight.FuelConsumption,
                NameAircraft = aircraft.Name,
                NameAirportArrive = airportArrive.Name,
                NameAirportDeparture = airportDeparture.Name,
                StateArrive = airportArrive.State,
                StateDeparture = airportDeparture.State
            };

            return await flightRepository.UpdateAsync(flightDto.ToModel());
        }
    }
}
