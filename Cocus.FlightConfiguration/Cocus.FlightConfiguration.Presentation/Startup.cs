﻿namespace Cocus.FlightConfiguration.Presentation
{
    using Cocus.FlightConfiguration.Crosscutting.Configuration;
    using Cocus.FlightConfiguration.Crosscutting.IoC;
    using Cocus.FlightConfiguration.Presentation.Setup;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Swashbuckle.AspNetCore.SwaggerUI;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public IConfiguration configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var applicationSettings = this.configuration.GetSection(nameof(ApplicationSettings)).Get<ApplicationSettings>();
            services.AddSingleton<IApplicationSettings>(applicationSettings);

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services
                .AddMongoDb(applicationSettings)
                .AddSwaggerGen(s =>
                {
                    s.SwaggerDoc("v1",
                        new Swashbuckle.AspNetCore.Swagger.Info { Title = "Flight Configuration", Version = "V1" });
                    s.DescribeAllEnumsAsStrings();
                })
                .AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            RegisterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationSettings applicationSettings)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app
            .UseMongoDb(applicationSettings)
                .UseSwagger()
                .UseSwaggerUI(config =>
                {
                    config.SwaggerEndpoint("/swagger/v1/swagger.json", "Flight Configuration V1");
                    config.DocExpansion(DocExpansion.None);
                })
                .UseMvc();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private static void RegisterServices(IServiceCollection services)
        {
            // Adding dependencies from another layers (isolated from Presentation)
            IoC.RegisterServices(services);
        }

        private void OnApplicationStopped(IApplicationBuilder app)
        {
            app.StopMongoDb();
        }
    }
}
