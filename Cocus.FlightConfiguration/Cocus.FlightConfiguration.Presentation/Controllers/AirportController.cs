﻿namespace Cocus.FlightConfiguration.Presentation.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;

    public class AirportController : Controller
    {
        private readonly IAirportService airportService;

        public AirportController(IAirportService airportService)
        {
            this.airportService = airportService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int skip = 0, int take = 10)
        {
            var result = await airportService.GetAllAsync(skip * 10, take).ConfigureAwait(false);

            ViewData["TotalPages"] = result.TotalPages;
            ViewData["Number"] = result.Number;

            return View(result.Entries);
        }

        [HttpGet]
        public IEnumerable<AirportDTO> GetPartial(string partialText)
        {
            var result = airportService.GetPartial(partialText);

            return result;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AirportDTO airportDTO)
        {
            if (!ModelState.IsValid) return View(airportDTO);

            try
            {
                airportService.CreateAsync(airportDTO);
                ViewBag.Sucesso = "Airport Registered!";
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Airport not Registered!";
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var airportDTO = await airportService.GetByIdAsync(id.Value);

            if (airportDTO == null)
            {
                return NotFound();
            }

            return View(airportDTO);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                airportService.DeleteAsync(id);
                ViewBag.Sucesso = "Airport Deleted!";
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Airport not Deleted!";
                return View(airportService.GetByIdAsync(id));
            }

            return RedirectToAction("Index");
        }
    }
}