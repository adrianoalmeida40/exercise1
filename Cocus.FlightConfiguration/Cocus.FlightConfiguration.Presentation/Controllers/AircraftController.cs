﻿namespace Cocus.FlightConfiguration.Presentation.Controllers
{
    using System;
    using System.Collections;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;

    public class AircraftController : Controller
    {
        private readonly IAircraftService aircraftService;

        public AircraftController(IAircraftService aircraftService)
        {
            this.aircraftService = aircraftService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await aircraftService.GetAllAsync().ConfigureAwait(false));
        }

        [HttpGet]
        public IActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aircraftDTO = aircraftService.GetByIdAsync(id.Value);

            if (aircraftDTO == null)
            {
                return NotFound();
            }

            return View(aircraftDTO);
        }

        [HttpGet]
        public IActionResult Create()
        {            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AircraftDTO aircraftDTO)
        {
            if (!ModelState.IsValid) return View(aircraftDTO);

            try
            {
                aircraftService.CreateAsync(aircraftDTO);
                ViewBag.Sucesso = "Aircraft Registered!";
            }
            catch(Exception ex)
            {
                ViewBag.Error = "Aircraft not Registered!";
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aircraftDTO = aircraftService.GetByIdAsync(id.Value);

            if (aircraftDTO == null)
            {
                return NotFound();
            }

            return View(aircraftDTO);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(AircraftDTO aircraftDTO)
        {
            if (!ModelState.IsValid) return View(aircraftDTO);

            try
            {
                aircraftService.UpdateAsync(aircraftDTO);
                ViewBag.Sucesso = "Aircraft Updated!";
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Aircraft not Updated!";
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aircraftDTO = await aircraftService.GetByIdAsync(id.Value);

            if (aircraftDTO == null)
            {
                return NotFound();
            }

            return View(aircraftDTO);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                aircraftService.DeleteAsync(id);
                ViewBag.Sucesso = "Aircraft Deleted!";
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Aircraft not Deleted!";
                return View(aircraftService.GetByIdAsync(id));
            }

            return RedirectToAction("Index");
        }
    }
}