﻿namespace Cocus.FlightConfiguration.Presentation.Controllers
{
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using System;
    using System.Collections;
    using System.Threading.Tasks;

    public class FlightController : Controller
    {
        private readonly IFlightService flightService;
        private readonly IAircraftService aircraftService;
        private readonly IAirportService airportService;

        public FlightController(IFlightService flightService,
                                IAircraftService aircraftService,
                                IAirportService airportService)
        {
            this.flightService = flightService;
            this.aircraftService = aircraftService;
            this.airportService = airportService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int skip = 0, int take = 10)
        {
            var result = await flightService.GetAllAsync(skip * 10, take).ConfigureAwait(false);

            ViewData["TotalPages"] = result.TotalPages;
            ViewData["Number"] = result.Number;

            return View(result.Entries);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var aircrafts = await aircraftService.GetAllAsync().ConfigureAwait(false);
            var airports = await airportService.GetAllAsync(0, 10000).ConfigureAwait(false);
            ViewData["Aircraft"] = new SelectList((IEnumerable) aircrafts, "Id", "Name");
            ViewData["Airport"] = new SelectList((IEnumerable) airports.Entries, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(FlightCreateDTO flightCreateDTO)
        {
            if (!ModelState.IsValid) return View(flightCreateDTO);

            try
            {
                flightService.CreateAsync(flightCreateDTO);
                ViewBag.Sucesso = "Flight Registered!";
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Flight not Registered!";
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var flightDTO = await flightService.GetByIdAsync(id.Value).ConfigureAwait(false);

            if (flightDTO == null)
            {
                return NotFound();
            }

            return View(flightDTO);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(FlightDTO flightDTO)
        {
            if (!ModelState.IsValid) return View(flightDTO);

            try
            {
                flightService.UpdateAsync(flightDTO);
                ViewBag.Sucesso = "Flight Updated!";
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Flight not Updated!";
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var airportDTO = await flightService.GetByIdAsync(id.Value);

            if (airportDTO == null)
            {
                return NotFound();
            }

            return View(airportDTO);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                flightService.DeleteAsync(id);
                ViewBag.Sucesso = "Flight Deleted!";
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Flight not Deleted!";
                return View(flightService.GetByIdAsync(id));
            }

            return RedirectToAction("Index");
        }
    }
}