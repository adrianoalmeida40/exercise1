﻿namespace Cocus.FlightConfiguration.Api.Controllers
{
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    [Route("v1/[controller]")]
    public class AirportController : Controller
    {
        private readonly IAirportService airportService;

        public AirportController(IAirportService airportService)
        {
            this.airportService = airportService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(AirportDTO))]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
        {
            var aircraftDTO = await airportService.GetByIdAsync(id).ConfigureAwait(false);

            if (aircraftDTO == null)
            {
                return this.NotFound();
            }

            return this.Ok(aircraftDTO);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateAsync([FromBody] AirportDTO airportDTO)
        {
            var createdaircraftDto = await airportService.CreateAsync(airportDTO).ConfigureAwait(false);

            return this.Created(createdaircraftDto.Id.ToString(), createdaircraftDto);
        }

        [HttpPost]
        [Route("CreateMany")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateManyAsync([FromBody] IEnumerable<AirportDTO> airportDTO)
        {
            var createdAirports = await airportService.CreateManyAsync(airportDTO).ConfigureAwait(false);

            return this.Ok(createdAirports);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateAsync([FromRoute] Guid id, [FromBody] AirportDTO airportDTO)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.NotFound();
            }

            airportDTO.Id = id;

            await airportService.UpdateAsync(airportDTO).ConfigureAwait(false);

            return this.Ok();
        }
        
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<AirportDTO>))]
        public IEnumerable<AirportDTO> GetPartial([FromQuery] string partialText)
        {
            var airports = airportService.GetPartial(partialText);

            if (airports == null)
            {
                return new List<AirportDTO>();
            }

            return airports;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<AirportDTO>))]
        public async Task<IActionResult> GetAllAsync([FromQuery] int skip = Pagination.Skip,
                                                     [FromQuery] int take = Pagination.Take)
        {
            var pagedAirportsDTO = await airportService.GetAllAsync(skip, take).ConfigureAwait(false);

            if (pagedAirportsDTO == null)
            {
                return this.Ok(new List<AirportDTO>());
            }

            return this.Ok(pagedAirportsDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.BadRequest("The Id cannot be empty");
            }

            await airportService.DeleteAsync(id).ConfigureAwait(false);

            return this.Ok();
        }
    }
}