﻿namespace Cocus.FlightConfiguration.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;

    [Route("v1/[controller]")]
    public class FlightController : Controller
    {
        private readonly IFlightService flightService;

        public FlightController(IFlightService flightService)
        {
            this.flightService = flightService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(FlightDTO))]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
        {
            var flightDTO = await flightService.GetByIdAsync(id).ConfigureAwait(false);

            if (flightDTO == null)
            {
                return this.NotFound();
            }

            return this.Ok(flightDTO);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateAsync([FromBody] FlightCreateDTO flightCreateDTO)
        {
            var createdFlightDto = await flightService.CreateAsync(flightCreateDTO).ConfigureAwait(false);

            return this.Created(createdFlightDto.Id.ToString(), createdFlightDto);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateAsync([FromRoute] Guid id, [FromBody] FlightDTO flightDTO)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.NotFound();
            }

            flightDTO.Id = id;

            await flightService.UpdateAsync(flightDTO).ConfigureAwait(false);

            return this.Ok();
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<FlightDTO>))]
        public async Task<IActionResult> GetAllAsync([FromQuery] int skip = Pagination.Skip,
                                                     [FromQuery] int take = Pagination.Take)
        {
            var flightsDTO = await flightService.GetAllAsync(skip, take).ConfigureAwait(false);

            if (flightsDTO == null)
            {
                return this.Ok(new List<FlightDTO>());
            }

            return this.Ok(flightsDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.BadRequest("The Id cannot be empty");
            }

            await flightService.DeleteAsync(id).ConfigureAwait(false);

            return this.Ok();
        }
    }
}