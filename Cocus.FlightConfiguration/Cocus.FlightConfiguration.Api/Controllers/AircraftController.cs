﻿namespace Cocus.FlightConfiguration.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Application.DTO;
    using Cocus.FlightConfiguration.Application.Interfaces;
    using Microsoft.AspNetCore.Mvc;

    [Route("v1/[controller]")]
    public class AircraftController : Controller
    {
        private readonly IAircraftService aircraftService;

        public AircraftController(IAircraftService aircraftService)
        {
            this.aircraftService = aircraftService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(AircraftDTO))]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
        {
            var aircraftDTO = await aircraftService.GetByIdAsync(id).ConfigureAwait(false);

            if (aircraftDTO == null)
            {
                return this.NotFound();
            }

            return this.Ok(aircraftDTO);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateAsync([FromBody] AircraftDTO aircraftDTO)
        {
            var createdaircraftDto = await aircraftService.CreateAsync(aircraftDTO).ConfigureAwait(false);

            return this.Created(createdaircraftDto.Id.ToString(), createdaircraftDto);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateAsync([FromRoute] Guid id, [FromBody] AircraftDTO aircraftDTO)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.NotFound();
            }

            aircraftDTO.Id = id;

            await aircraftService.UpdateAsync(aircraftDTO).ConfigureAwait(false);

            return this.Ok();
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<AircraftDTO>))]
        public async Task<IActionResult> GetAllAsync()
        {
            var aircraftsDTO = await aircraftService.GetAllAsync().ConfigureAwait(false);

            if (aircraftsDTO == null)
            {
                return this.Ok(new List<AircraftDTO>());
            }

            return this.Ok(aircraftsDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return this.BadRequest("The Id cannot be empty");
            }

            await aircraftService.DeleteAsync(id).ConfigureAwait(false);

            return this.Ok();
        }
    }
}