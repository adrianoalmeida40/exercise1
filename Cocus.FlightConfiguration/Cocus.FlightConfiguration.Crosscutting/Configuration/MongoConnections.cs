﻿namespace Cocus.FlightConfiguration.Crosscutting.Configuration
{
    using Cocus.FlightConfiguration.Infra.Data.Configuration;

    public class MongoConnections
    {
        public MongoSettings FlightConfiguration { get; set; }
    }
}
