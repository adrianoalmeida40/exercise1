﻿namespace Cocus.FlightConfiguration.Crosscutting.Configuration
{
    public interface IApplicationSettings
    {
        MongoConnections Mongo { get; }
    }
}
