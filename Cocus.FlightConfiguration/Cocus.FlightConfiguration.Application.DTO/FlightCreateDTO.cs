﻿namespace Cocus.FlightConfiguration.Application.DTO
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class FlightCreateDTO
    {
        [Display(Name = "Departure")]
        public Guid AirportDepartureId { get; set; }

        [Display(Name = "Arrive")]
        public Guid AirportArriveId { get; set; }

        [Display(Name = "Aircraft")]
        public Guid AircraftId { get; set; }

        public double DistancieAirports { get; set; }

        public double FuelConsumption { get; set; }
    }
}
