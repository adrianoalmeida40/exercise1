﻿namespace Cocus.FlightConfiguration.Application.DTO
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AircraftDTO
    {
        public Guid Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "Consumption per Km")]
        public double ConsumptionPerKm { get; set; }

        [Display(Name = "Takeoff Effort")]
        public double TakeoffEffort { get; set; }

        [Display(Name = "Average Speed")]
        public decimal AverageSpeed { get; set; }
    }
}
