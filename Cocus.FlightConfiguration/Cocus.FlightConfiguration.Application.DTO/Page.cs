﻿namespace Cocus.FlightConfiguration.Application.DTO
{
    using System;
    using System.Collections.Generic;

    public class Page<T>
    {
        public Page()
        {
            this.Number = 1;
            this.TotalPages = 1;
        }

        public Page(IEnumerable<T> content, int skip, int take, long total)
        {
            this.Number = take > 0 ? (skip / take) + 1 : 1;
            this.TotalPages = take > 0 ? (int)Math.Ceiling((double)total / take) : 1;
            this.TotalItems = total;
            this.Entries = content;
        }

        public int Number { get; set; }

        public int TotalPages { get; set; }

        public long TotalItems { get; set; }

        public IEnumerable<T> Entries { get; set; }
    }
}
