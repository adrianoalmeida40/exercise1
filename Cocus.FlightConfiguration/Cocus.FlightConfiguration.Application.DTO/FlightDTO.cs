﻿namespace Cocus.FlightConfiguration.Application.DTO
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class FlightDTO
    {
        public Guid Id { get; set; }

        [Display(Name = "Departure")]
        public Guid AirportDepartureId { get; set; }

        [Display(Name = "Departure")]
        public string NameAirportDeparture { get; set; }

        [Display(Name = "Arrive")]
        public Guid AirportArriveId { get; set; }

        [Display(Name = "Arrive")]
        public string NameAirportArrive { get; set; }

        [Display(Name = "Distance (km)")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double DistancieAirports { get; set; }

        [Display(Name = "Fuel")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double FuelConsumption { get; set; }

        [Display(Name = "Aircraft")]
        public Guid AircraftId { get; set; }

        [Display(Name = "Aircraft")]
        public string NameAircraft { get; set; }

        [Display(Name = "Code Arrive")]
        public string AirportArriveCode { get; set; }

        [Display(Name = "Code Departure")]
        public string AirportDepartureCode { get; set; }

        public string CityArrive { get; set; }

        public string CityDeparture { get; set; }

        public string StateArrive { get; set; }

        public string StateDeparture { get; set; }

        public string CountryArrive { get; set; }

        public string CountryDeparture { get; set; }
    }
}
