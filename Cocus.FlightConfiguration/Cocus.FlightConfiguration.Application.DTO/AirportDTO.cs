﻿namespace Cocus.FlightConfiguration.Application.DTO
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AirportDTO
    {
        public Guid Id { get; set; }

        [Display(Name = "Code")]
        public string Code { get; set; }

        [Display(Name = "Latitude")]
        public double Latitude { get; set; }

        [Display(Name = "Longitude")]
        public double Longitude { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }
        public long? Woeid { get; set; }
        public string Tz { get; set; }
        public string Phone { get; set; }
        public string Type { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string Runway_Lenght { get; set; }
        public decimal? Elev { get; set; }
        public string ICAO { get; set; }
        public string Direct_Flights { get; set; }
        public decimal? Carries { get; set; }
    }
}
