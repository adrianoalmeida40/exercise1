﻿namespace Cocus.FlightConfiguration.Application.DTO
{
    public class Pagination
    {
        public const int Take = 400;

        public const int Skip = 0;
    }
}
