﻿namespace Cocus.FlightConfiguration.Infra.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Domain.Models;

    public interface IAirportRepository
    {
        Task<Airport> GetByIdAsync(Guid id);

        Task<IEnumerable<Airport>> GetAllAsync(int skip, int take);

        IEnumerable<Airport> GetPartial(string partialText);

        Task<Airport> CreateAsync(Airport airport);

        Task<bool> CreateManyAsync(IEnumerable<Airport> airports);

        Task<bool> UpdateAsync(Airport airport);

        Task<bool> DeleteAsync(Guid id);

        Task<long> Count();
    }
}
