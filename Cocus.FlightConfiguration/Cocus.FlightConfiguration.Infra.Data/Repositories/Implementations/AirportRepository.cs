﻿namespace Cocus.FlightConfiguration.Infra.Data.Repositories.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Domain.Models;
    using Cocus.FlightConfiguration.Infra.Data.Connection;
    using MongoDB.Driver;

    public class AirportRepository : BaseRepository<Airport>, IAirportRepository
    {
        public AirportRepository(IMongoDbConnection mongoDbConnection)
            : base(mongoDbConnection, "airport")
        {

        }

        public Task<long> Count()
        {
            return this.GetCollection().CountDocumentsAsync(_ => true);
        }

        public async Task<Airport> CreateAsync(Airport airport)
        {
            airport.Id = Guid.NewGuid();

            await this.GetCollection().InsertOneAsync(airport).ConfigureAwait(false);

            return airport;
        }

        public async Task<bool> CreateManyAsync(IEnumerable<Airport> airports)
        {
            await this.GetCollection().InsertManyAsync(airports).ConfigureAwait(false);

            return true;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var filter = Builders<Airport>.Filter.Eq(airport => airport.Id, id);

            var deleteResult = await this.GetCollection().DeleteManyAsync(filter).ConfigureAwait(false);

            return deleteResult.DeletedCount >= 0;
        }

        public async Task<IEnumerable<Airport>> GetAllAsync(int skip, int take)
        {
            return await this.GetCollection().Find(_ => true).Skip(skip).Limit(take).ToListAsync().ConfigureAwait(false);
        }

        public async Task<Airport> GetByIdAsync(Guid id)
        {
            var filter = Builders<Airport>.Filter.Eq(airport => airport.Id, id);

            var result = await this.GetCollection().FindAsync(filter).ConfigureAwait(false);

            return await result.SingleOrDefaultAsync().ConfigureAwait(false);
        }

        public IEnumerable<Airport> GetPartial(string partialText)
        {
            var filter = Builders<Airport>.Filter.Where(airport => airport.Code.Contains(partialText) ||
                                                        airport.City.Contains(partialText) ||
                                                        airport.Name.Contains(partialText));

            var result = this.GetCollection().Find(filter).ToList();

            return result;
        }

        public async Task<bool> UpdateAsync(Airport airport)
        {
            var filter = Builders<Airport>.Filter.Eq(airportModel => airportModel.Id, airport.Id);

            var updateBuilder = Builders<Airport>.Update
                                    .Set(airportUpdate => airportUpdate.City, airport.City)
                                    .Set(airportUpdate => airportUpdate.Country, airport.Country)
                                    .Set(airportUpdate => airportUpdate.ICAO, airport.ICAO)
                                    .Set(airportUpdate => airportUpdate.Latitude, airport.Latitude)
                                    .Set(airportUpdate => airportUpdate.Longitude, airport.Longitude)
                                    .Set(airportUpdate => airportUpdate.Name, airport.Name)
                                    .Set(airportUpdate => airportUpdate.Type, airport.Type)
                                    .Set(airportUpdate => airportUpdate.Carries, airport.Carries)
                                    .Set(airportUpdate => airportUpdate.Code, airport.Code)
                                    .Set(airportUpdate => airportUpdate.Direct_Flights, airport.Direct_Flights)
                                    .Set(airportUpdate => airportUpdate.Elev, airport.Elev)
                                    .Set(airportUpdate => airportUpdate.Email, airport.Email)
                                    .Set(airportUpdate => airportUpdate.Phone, airport.Phone)
                                    .Set(airportUpdate => airportUpdate.Runway_Lenght, airport.Runway_Lenght)
                                    .Set(airportUpdate => airportUpdate.State, airport.State)
                                    .Set(airportUpdate => airportUpdate.Tz, airport.Tz)
                                    .Set(airportUpdate => airportUpdate.Url, airport.Url)
                                    .Set(airportUpdate => airportUpdate.Woeid, airport.Woeid);

            var result = await this.GetCollection().UpdateOneAsync(filter, updateBuilder).ConfigureAwait(false);

            return result.ModifiedCount > 0;
        }
    }
}
