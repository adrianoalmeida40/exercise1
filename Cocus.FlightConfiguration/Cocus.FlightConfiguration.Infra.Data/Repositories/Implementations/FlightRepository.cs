﻿namespace Cocus.FlightConfiguration.Infra.Data.Repositories.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Domain.Models;
    using Cocus.FlightConfiguration.Infra.Data.Connection;
    using MongoDB.Driver;

    public class FlightRepository : BaseRepository<Flight>, IFlightRepository
    {
        public FlightRepository(IMongoDbConnection mongoDbConnection)
            : base(mongoDbConnection, "flight")
        {

        }

        public Task<long> Count()
        {
            return this.GetCollection().CountDocumentsAsync(_ => true);
        }

        public async Task<Flight> CreateAsync(Flight flight)
        {
            flight.Id = Guid.NewGuid();

            await this.GetCollection().InsertOneAsync(flight).ConfigureAwait(false);

            return flight;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var filter = Builders<Flight>.Filter.Eq(flight => flight.Id, id);

            var deleteResult = await this.GetCollection().DeleteManyAsync(filter).ConfigureAwait(false);

            return deleteResult.DeletedCount >= 0;
        }

        public async Task<bool> DeleteForAircraftAsync(Guid aircraftId)
        {
            var filter = Builders<Flight>.Filter.Eq(flight => flight.AircraftId, aircraftId);

            var deleteResult = await this.GetCollection().DeleteManyAsync(filter).ConfigureAwait(false);

            return deleteResult.DeletedCount >= 0;
        }

        public async Task<bool> DeleteForAirportAsync(Guid airportId)
        {
            var filter = Builders<Flight>.Filter.Where(flight => flight.AirportArriveId == airportId ||
                                                                 flight.AirportDepartureId == airportId);

            var deleteResult = await this.GetCollection().DeleteManyAsync(filter).ConfigureAwait(false);

            return deleteResult.DeletedCount >= 0;
        }

        public async Task<IEnumerable<Flight>> GetAllAsync(int skip, int take)
        {
            return await this.GetCollection().Find(_ => true).Skip(skip).Limit(take).ToListAsync().ConfigureAwait(false);
        }

        public async Task<Flight> GetByIdAsync(Guid id)
        {
            var filter = Builders<Flight>.Filter.Eq(flight => flight.Id, id);

            var result = await this.GetCollection().FindAsync(filter).ConfigureAwait(false);

            return await result.SingleOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task<bool> UpdateAsync(Flight flight)
        {
            var filter = Builders<Flight>.Filter.Eq(flightModel => flightModel.Id, flight.Id);

            var updateBuilder = Builders<Flight>.Update
                                    .Set(flightUpdate => flightUpdate.AircraftId, flight.AircraftId)
                                    .Set(flightUpdate => flightUpdate.AirportArriveId, flight.AirportArriveId)
                                    .Set(flightUpdate => flightUpdate.AirportDepartureId, flight.AirportDepartureId)
                                    .Set(flightUpdate => flightUpdate.DistancieAirports, flight.DistancieAirports)
                                    .Set(flightUpdate => flightUpdate.FuelConsumption, flight.FuelConsumption)
                                    .Set(flightUpdate => flightUpdate.NameAircraft, flight.NameAircraft)
                                    .Set(flightUpdate => flightUpdate.NameAirportArrive, flight.NameAirportArrive)
                                    .Set(flightUpdate => flightUpdate.NameAirportDeparture, flight.NameAirportDeparture);

            var result = await this.GetCollection().UpdateOneAsync(filter, updateBuilder).ConfigureAwait(false);

            return result.ModifiedCount > 0;
        }
    }
}
