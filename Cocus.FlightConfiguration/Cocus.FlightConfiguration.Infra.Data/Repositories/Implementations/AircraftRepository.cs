﻿namespace Cocus.FlightConfiguration.Infra.Data.Repositories.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Domain.Models;
    using Cocus.FlightConfiguration.Infra.Data.Connection;
    using MongoDB.Driver;

    public class AircraftRepository : BaseRepository<Aircraft>, IAircraftRepository
    {
        public AircraftRepository(IMongoDbConnection mongoDbConnection)
            : base(mongoDbConnection, "aircraft")
        {

        }

        public async Task<Aircraft> CreateAsync(Aircraft aircraft)
        {
            aircraft.Id = Guid.NewGuid();

            await this.GetCollection().InsertOneAsync(aircraft).ConfigureAwait(false);

            return aircraft;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var filter = Builders<Aircraft>.Filter.Eq(aircraft => aircraft.Id, id);

            var deleteResult = await this.GetCollection().DeleteManyAsync(filter).ConfigureAwait(false);

            return deleteResult.DeletedCount >= 0;
        }

        public async Task<IEnumerable<Aircraft>> GetAllAsync()
        {
            return await this.GetCollection().Find(_ => true).ToListAsync().ConfigureAwait(false);
        }

        public async Task<Aircraft> GetByIdAsync(Guid id)
        {
            var filter = Builders<Aircraft>.Filter.Eq(aircraft => aircraft.Id, id);

            var result = await this.GetCollection().FindAsync(filter).ConfigureAwait(false);

            return await result.SingleOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task<bool> UpdateAsync(Aircraft aircraft)
        {
            var filter = Builders<Aircraft>.Filter.Eq(aircraftModel => aircraftModel.Id, aircraft.Id);

            var updateBuilder = Builders<Aircraft>.Update
                                    .Set(aircraftUpdate => aircraftUpdate.AverageSpeed, aircraft.AverageSpeed)
                                    .Set(aircraftUpdate => aircraftUpdate.ConsumptionPerKm, aircraft.ConsumptionPerKm)
                                    .Set(aircraftUpdate => aircraftUpdate.Name, aircraft.Name)
                                    .Set(aircraftUpdate => aircraftUpdate.TakeoffEffort, aircraft.TakeoffEffort);

            var result = await this.GetCollection().UpdateOneAsync(filter, updateBuilder).ConfigureAwait(false);

            return result.ModifiedCount > 0;
        }
    }
}
