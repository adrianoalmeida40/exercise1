﻿namespace Cocus.FlightConfiguration.Infra.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Domain.Models;

    public interface IAircraftRepository
    {
        Task<Aircraft> GetByIdAsync(Guid id);

        Task<IEnumerable<Aircraft>> GetAllAsync();

        Task<Aircraft> CreateAsync(Aircraft aircraft);

        Task<bool> UpdateAsync(Aircraft aircraft);

        Task<bool> DeleteAsync(Guid id);
    }
}
