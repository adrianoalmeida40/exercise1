﻿namespace Cocus.FlightConfiguration.Infra.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Cocus.FlightConfiguration.Domain.Models;

    public interface IFlightRepository
    {
        Task<Flight> GetByIdAsync(Guid id);

        Task<IEnumerable<Flight>> GetAllAsync(int skip, int take);

        Task<Flight> CreateAsync(Flight flight);

        Task<bool> UpdateAsync(Flight flight);

        Task<bool> DeleteAsync(Guid id);

        Task<bool> DeleteForAircraftAsync(Guid aircraftId);

        Task<bool> DeleteForAirportAsync(Guid airportId);

        Task<long> Count();
    }
}
