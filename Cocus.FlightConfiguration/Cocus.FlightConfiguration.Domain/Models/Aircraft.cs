﻿namespace Cocus.FlightConfiguration.Domain.Models
{
    using System;
    using MongoDB.Bson.Serialization.Attributes;

    public class Aircraft
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public double ConsumptionPerKm { get; set; }
        public double TakeoffEffort { get; set; }
        public decimal AverageSpeed { get; set; }
    }
}
