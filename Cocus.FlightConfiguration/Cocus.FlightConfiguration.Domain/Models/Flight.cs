﻿namespace Cocus.FlightConfiguration.Domain.Models
{
    using System;
    using MongoDB.Bson.Serialization.Attributes;

    public class Flight
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid Id { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid AirportDepartureId { get; set; }

        public string NameAirportDeparture { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid AirportArriveId { get; set; }

        public string NameAirportArrive { get; set; }

        public double DistancieAirports { get; set; }

        public double FuelConsumption { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid AircraftId { get; set; }

        public string NameAircraft { get; set; }

        public string AirportArriveCode { get; set; }

        public string AirportDepartureCode { get; set; }

        public string CityArrive { get; set; }

        public string CityDeparture { get; set; }

        public string StateArrive { get; set; }

        public string StateDeparture { get; set; }

        public string CountryArrive { get; set; }

        public string CountryDeparture { get; set; }
    }
}
