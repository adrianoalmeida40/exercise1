﻿namespace Cocus.FlightConfiguration.Domain.Models
{
    using System;
    using MongoDB.Bson.Serialization.Attributes;

    public class Airport
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid Id { get; set; }
        public string Code { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public long? Woeid { get; set; }
        public string Tz { get; set; }
        public string Phone { get; set; }
        public string Type { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string Runway_Lenght { get; set; }
        public decimal? Elev { get; set; }
        public string ICAO { get; set; }
        public string Direct_Flights { get; set; }
        public decimal? Carries { get; set; }
    }
}
